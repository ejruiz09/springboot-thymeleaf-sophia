package com.sophiahotel;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.sophiahotel.controller.GuestController;
import com.sophiahotel.model.Guest;
import com.sophiahotel.service.GuestService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GuestController.class)
@AutoConfigureMockMvc
public class GuestControllerTest {

	@MockBean
	public GuestService guestService;
	public Guest guest;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void viewGuestList() throws Exception {
		Guest guest1 = new Guest();
		guest1.setId(8);
		guest1.setFirstName("Edward");
		guest1.setLastName("Ruiz");
		guest1.setMemberSince("2021-12-01");
		Guest guest2 = new Guest();
		guest2.setId(9);
		guest2.setFirstName("Katrina");
		guest2.setLastName("Lamery");
		guest2.setMemberSince("2021-12-02");
		when(guestService.listAll()).thenReturn(Arrays.asList(guest1, guest2));
		this.mvc.perform(get("/guest")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void newGuest() throws Exception {
		this.mvc.perform(get("/new")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void editGuest() throws Exception {
		this.mvc.perform(get("/edit/9")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void saveGuest() throws Exception {
		Guest guest1 = new Guest();
		guest1.setId(8);
		guest1.setFirstName("Edward");
		guest1.setLastName("Ruiz");
		guest1.setMemberSince("2021-12-01");
		this.mvc.perform(post("/save/guest1")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
	}
	
	@Test
	public void saveGuest1() throws Exception {
		this.mvc.perform(post("/save/guest1")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
	}
	
	@Test
	public void deleteGuest() throws Exception {
		this.mvc.perform(get("/delete/4")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
	}
	
}
