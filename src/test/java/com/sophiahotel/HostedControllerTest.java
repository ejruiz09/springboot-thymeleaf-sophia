package com.sophiahotel;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.sophiahotel.controller.HostedAtController;
import com.sophiahotel.model.HostedAt;
import com.sophiahotel.service.HostedAtService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = HostedAtController.class)
@AutoConfigureMockMvc
public class HostedControllerTest {
	
	@MockBean
	public HostedAtService hostedAtService;
	public HostedAt hostedAt;
	
	@Autowired
	private MockMvc mvc;
	
	@Test
	public void viewHostedAtList() throws Exception {
		HostedAt hostedAt1 = new HostedAt();
		hostedAt1.setId(8);
		hostedAt1.setGuestId(8);
		hostedAt1.setOccupiedRoomId(44);
		HostedAt hostedAt2 = new HostedAt();
		hostedAt2.setId(9);
		hostedAt2.setGuestId(9);
		hostedAt2.setOccupiedRoomId(45);
		when(hostedAtService.listAll()).thenReturn(Arrays.asList(hostedAt1, hostedAt2));
		this.mvc.perform(get("/hostedAt")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void newHost() throws Exception {
		this.mvc.perform(get("/newHost")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void editHost() throws Exception {
		this.mvc.perform(get("/editHost/9")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(status().isOk());
	}
	
	@Test
	public void saveHost() throws Exception {
		HostedAt hostedAt = new HostedAt();
		hostedAt.setId(9);
		hostedAt.setGuestId(9);
		hostedAt.setOccupiedRoomId(45);
		this.mvc.perform(post("/saveHost/hostedAt")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
	}
	
	@Test
	public void saveHost1() throws Exception {
		this.mvc.perform(post("/saveHost/hostedAt")
				.contentType(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();
	}
	
	@Test
	public void deleteHost() throws Exception {
		this.mvc.perform(get("/deleteHost/5")
			.contentType(MediaType.APPLICATION_JSON_VALUE))
			.andReturn();
	}	
	
	

}
