package com.sophiahotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reserved_room")
public class ReservedRoom {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "number_of_rooms")
	private int numberOfRooms;
	
	@Column(name = "room_type_id")
	private int roomTypeId;

	@Column(name = "reservation_id")
	private int reservationId;
	
	@Column(name = "status")
	private String status;

	public ReservedRoom() {
		
	}

	public ReservedRoom(int id, int numberOfRooms, int roomTypeId, int reservationId, String status) {
		super();
		this.id = id;
		this.numberOfRooms = numberOfRooms;
		this.roomTypeId = roomTypeId;
		this.reservationId = reservationId;
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumberOfRooms() {
		return numberOfRooms;
	}

	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	public int getRoomTypeId() {
		return roomTypeId;
	}

	public void setRoomTypeId(int roomTypeId) {
		this.roomTypeId = roomTypeId;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "ReservedRoom [id=" + id + ", numberOfRooms=" + numberOfRooms + ", roomTypeId=" + roomTypeId
				+ ", reservationId=" + reservationId + ", status=" + status + "]";
	}
	
	
	
	
}
