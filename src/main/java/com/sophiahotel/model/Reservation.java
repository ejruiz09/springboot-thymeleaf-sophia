package com.sophiahotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "reservation")
public class Reservation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "date_in")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateIn;

	@Column(name = "date_out")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String dateOut;

	@Column(name = "made_by")
	private String madeBy;

	@Column(name = "guest_id")
	private int guestId;

	public Reservation() {

	}

	public Reservation(int id, String dateIn, String dateOut, String madeBy, int guestId) {
		super();
		this.id = id;
		this.dateIn = dateIn;
		this.dateOut = dateOut;
		this.madeBy = madeBy;
		this.guestId = guestId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDateIn() {
		return dateIn;
	}

	public void setDateIn(String dateIn) {
		this.dateIn = dateIn;
	}

	public String getDateOut() {
		return dateOut;
	}

	public void setDateOut(String dateOut) {
		this.dateOut = dateOut;
	}

	public String getMadeBy() {
		return madeBy;
	}

	public void setMadeBy(String madeBy) {
		this.madeBy = madeBy;
	}

	public int getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}

	@Override
	public String toString() {
		return "Reservation [id=" + id + ", dateIn=" + dateIn + ", dateOut=" + dateOut + ", madeBy=" + madeBy
				+ ", guestId=" + guestId + "]";
	}

}
