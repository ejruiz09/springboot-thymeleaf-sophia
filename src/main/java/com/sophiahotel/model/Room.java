package com.sophiahotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "room")
public class Room {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "number")
	private String number;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "smoke")
	private int smoke;
	
	@Column(name = "room_type_id")
	private int roomtypeid;

	public Room() {
		
	}

	public Room(int id, String number, String name, String status, int smoke, int roomtypeid) {
		super();
		this.id = id;
		this.number = number;
		this.name = name;
		this.status = status;
		this.smoke = smoke;
		this.roomtypeid = roomtypeid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getSmoke() {
		return smoke;
	}

	public void setSmoke(int smoke) {
		this.smoke = smoke;
	}

	public int getRoomtypeid() {
		return roomtypeid;
	}

	public void setRoomtypeid(int roomtypeid) {
		this.roomtypeid = roomtypeid;
	}

	@Override
	public String toString() {
		return "Room [id=" + id + ", number=" + number + ", name=" + name + ", status=" + status + ", smoke=" + smoke
				+ ", roomtypeid=" + roomtypeid + "]";
	}
	
	
	
	
}
