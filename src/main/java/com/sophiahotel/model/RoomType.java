package com.sophiahotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "room_type")
public class RoomType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "max_capacity")
	private int maxcapacity;

	public RoomType() {
		
	}

	public RoomType(int id, String description, int maxcapacity) {
		super();
		this.id = id;
		this.description = description;
		this.maxcapacity = maxcapacity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getMaxcapacity() {
		return maxcapacity;
	}

	public void setMaxcapacity(int maxcapacity) {
		this.maxcapacity = maxcapacity;
	}

	@Override
	public String toString() {
		return "RoomType [id=" + id + ", description=" + description + ", maxcapacity=" + maxcapacity + "]";
	}
	
	
	
	
}
