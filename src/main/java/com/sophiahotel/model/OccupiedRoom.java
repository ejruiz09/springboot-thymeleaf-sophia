package com.sophiahotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "occupied_room")
public class OccupiedRoom {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "check_in")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String checkIn;
	
	@Column(name = "check_out")
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private String checkOut;
	
	@Column(name = "room_id")
	private int roomId;
	
	@Column(name = "reservation_id")
	private int reservationId;

	public OccupiedRoom() {
		
	}

	public OccupiedRoom(int id, String checkIn, String checkOut, int roomId, int reservationId) {
		super();
		this.id = id;
		this.checkIn = checkIn;
		this.checkOut = checkOut;
		this.roomId = roomId;
		this.reservationId = reservationId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(String checkIn) {
		this.checkIn = checkIn;
	}

	public String getCheckOut() {
		return checkOut;
	}

	public void setCheckOut(String checkOut) {
		this.checkOut = checkOut;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public int getReservationId() {
		return reservationId;
	}

	public void setReservationId(int reservationId) {
		this.reservationId = reservationId;
	}

	@Override
	public String toString() {
		return "OccupiedRoom [id=" + id + ", checkIn=" + checkIn + ", checkOut=" + checkOut + ", roomId=" + roomId
				+ ", reservationId=" + reservationId + "]";
	}
	
	
	
	
	
}
