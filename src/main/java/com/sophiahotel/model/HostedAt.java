package com.sophiahotel.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hosted_at")
public class HostedAt {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	@Column(name = "guest_id")
	private int guestId;
	@Column(name = "occupied_room_id")
	private int occupiedRoomId;
	
	public HostedAt() {
		
	}

	public HostedAt(int id, int guestId, int occupiedRoomId) {
		super();
		this.id = id;
		this.guestId = guestId;
		this.occupiedRoomId = occupiedRoomId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGuestId() {
		return guestId;
	}

	public void setGuestId(int guestId) {
		this.guestId = guestId;
	}

	public int getOccupiedRoomId() {
		return occupiedRoomId;
	}

	public void setOccupiedRoomId(int occupiedRoomId) {
		this.occupiedRoomId = occupiedRoomId;
	}

	@Override
	public String toString() {
		return "HostedAt [id=" + id + ", guestId=" + guestId + ", occupiedRoomId=" + occupiedRoomId + "]";
	}
	
	
	
	
	
}
