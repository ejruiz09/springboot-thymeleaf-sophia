package com.sophiahotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sophiahotel.model.Room;
import com.sophiahotel.service.RoomService;

@Controller
public class RoomController {

	@Autowired
	private RoomService service;
	
	@RequestMapping("/room")
	public String viewRoomPage(Model model) {
		List<Room> listRoom = service.listAll();
		model.addAttribute("listRoom", listRoom);
		return "roomindex";
	}
	
	@RequestMapping("/newRoom")
	public String showRoom(Model model) {
		Room room = new Room();
		model.addAttribute("room", room);
		return "new_room";
	}
	
	@RequestMapping(value = "/saveRoom", method = RequestMethod.POST)
	public String saveRoom(@ModelAttribute("room") Room room) {
		service.save(room);
		return "redirect:/room";
	}
	
	@RequestMapping("/editRoom/{id}")
	public ModelAndView showEditRoom(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_room");
		Room room = service.get(id);
		mav.addObject("room", room);
		return mav;
	}
	
	@RequestMapping("/deleteRoom/{id}")
	public String deleteRoom(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/room";
	}
}
