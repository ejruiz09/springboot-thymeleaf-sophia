package com.sophiahotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sophiahotel.model.HostedAt;
import com.sophiahotel.service.HostedAtService;

@Controller
public class HostedAtController {

	@Autowired
	private HostedAtService service;
	
	@RequestMapping("/hostedAt")
	public String viewHostedAtPage(Model model) {
		List<HostedAt> listHostedAt = service.listAll();
		model.addAttribute("listHostedAt", listHostedAt);
		return "hostedAtindex";
	}
	
	@RequestMapping("/newHost")
	public String showHostedAtPage(Model model) {
		HostedAt hostedAt = new HostedAt();
		model.addAttribute("hostedAt", hostedAt);
		return "new_hostedAt";
	}
	
	@RequestMapping(value = "/saveHost", method = RequestMethod.POST)
	public String saveHostedAt(@ModelAttribute("hostedAt") HostedAt hostedAt) {
		service.save(hostedAt);
		
		return "redirect:/hostedAt";
	}
	
	@RequestMapping("/editHost/{id}")
	public ModelAndView showEditHostedAtPage(@PathVariable(name="id") int id) {
		ModelAndView mav = new ModelAndView("edit_hostedAt");
		HostedAt hostedAt = service.get(id);
		mav.addObject("hostedAt", hostedAt);
		
		return mav;
	}
	
	@RequestMapping("/deleteHost/{id}")
	public String deleteHostedAt(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/hostedAt";
	}
}
