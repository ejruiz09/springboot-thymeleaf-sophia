package com.sophiahotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sophiahotel.model.OccupiedRoom;
import com.sophiahotel.service.OccupiedRoomService;; 

@Controller
public class OccupiedRoomController {

	@Autowired
	private OccupiedRoomService service;
	
	@RequestMapping("/occupiedRoom")
	public String viewOccupiedRoomPage(Model model) {
		List<OccupiedRoom> listOccupiedRoom = service.listAll();
		model.addAttribute("listOccupiedRoom", listOccupiedRoom);
		return "occupiedRoomindex";
	}
	
	@RequestMapping("/newOccupiedRoom")
	public String showOccupiedRoom(Model model) {
		OccupiedRoom occupiedRoom = new OccupiedRoom();
		model.addAttribute("occupiedRoom", occupiedRoom);
		return "new_occupiedRoom";
	}
	
	@RequestMapping(value = "/saveOccupiedRoom", method = RequestMethod.POST)
	public String saveHostedAt(@ModelAttribute("occupiedRoom") OccupiedRoom occupiedRoom) {
		service.save(occupiedRoom);	
		return "redirect:/occupiedRoom";
	}
	
	@RequestMapping("/editOccupiedRoom/{id}")
	public ModelAndView showEditHostedAtPage(@PathVariable(name="id") int id) {
		ModelAndView mav = new ModelAndView("edit_occupiedRoom");
		OccupiedRoom occupiedRoom = service.get(id);
		mav.addObject("occupiedRoom", occupiedRoom);
		
		return mav;
	}
	
	@RequestMapping("/deleteOccupiedRoom/{id}")
	public String deleteOccupiedRoom(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/occupiedRoom";
	}
	
	
}
