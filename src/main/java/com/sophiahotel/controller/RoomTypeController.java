package com.sophiahotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.sophiahotel.model.RoomType;
import com.sophiahotel.service.RoomTypeService;

@Controller
public class RoomTypeController {

	@Autowired
	private RoomTypeService service;
	
	@RequestMapping("/roomtype")
	public String viewRoomTypePage(Model model) {
		List<RoomType> listRoomType = service.listAll();
		model.addAttribute("listRoomType", listRoomType);
		return "roomtypeindex";
	}
	
	@RequestMapping("/newRoomType")
	public String showRoomType(Model model) {
		RoomType roomType = new RoomType();
		model.addAttribute("roomType", roomType);
		return "new_roomtype";
	}
	
	@RequestMapping(value = "/saveRoomType", method = RequestMethod.POST)
	public String saveRoomType(@ModelAttribute("roomType") RoomType roomType) {
		service.save(roomType);
		return "redirect:/roomtype";
	}
	
	@RequestMapping("/editRoomType/{id}")
	public ModelAndView showEditRoomType(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_roomtype");
		RoomType roomType = service.get(id);
		mav.addObject("roomType", roomType);
		return mav;
	}
	
	@RequestMapping("/deleteRoomType/{id}")
	public String deleteRoomType(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/roomtype";
	}
	
}
