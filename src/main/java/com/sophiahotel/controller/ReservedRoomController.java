package com.sophiahotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sophiahotel.model.ReservedRoom;
import com.sophiahotel.service.ReservedRoomService;

@Controller
public class ReservedRoomController {

	@Autowired
	private ReservedRoomService service;
	
	@RequestMapping("/reservedroom")
	public String viewReservedRoomPage(Model model) {
		List<ReservedRoom> listReservedRoom = service.listAll();
		model.addAttribute("listReservedRoom", listReservedRoom);
		return "reservedroomindex";
	}
	
	@RequestMapping("/newReservedRoom")
	public String showReservedRoom(Model model) {
		ReservedRoom reservedroom = new ReservedRoom();
		model.addAttribute("reservedroom", reservedroom);
		return "new_reservedroom";
	}
	
	@RequestMapping(value ="/saveReservedRoom", method = RequestMethod.POST)
	public String saveReservedRoom(@ModelAttribute("reservedroom") ReservedRoom reservedroom) {
		service.save(reservedroom);
		return "redirect:/reservedroom";
	}
	
	@RequestMapping("/editReservedRoom/{id}")
	public ModelAndView showEditReservedRoom(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_reservedroom");
		ReservedRoom reservedroom = service.get(id);
		mav.addObject("reservedroom", reservedroom);
		return mav;
	}
	
	@RequestMapping("/deleteReservedRoom/{id}")
	public String deleteReservedRoom(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/reservedroom";
	}
}
