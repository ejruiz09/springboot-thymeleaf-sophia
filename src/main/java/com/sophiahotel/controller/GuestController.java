package com.sophiahotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sophiahotel.model.Guest;
import com.sophiahotel.service.GuestService;


@Controller
public class GuestController {

	@Autowired
	private GuestService service;
	
	@RequestMapping("/guest")
	public String viewGuestPage(Model model) {
		List<Guest> listGuest = service.listAll();
		model.addAttribute("listGuest", listGuest);
		return "guestindex";
	}
	
	@RequestMapping("/new")
	public String showNewGuestPage(Model model) {
		Guest guest = new Guest();
		model.addAttribute("guest", guest);
		return "new_guest";
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveGuest(@ModelAttribute("guest") Guest guest) {
		service.save(guest);
		
		return "redirect:/guest";
	}
	
	@RequestMapping("/edit/{id}")
	public ModelAndView showEditGuestPage(@PathVariable(name="id") int id) {
		ModelAndView mav = new ModelAndView("edit_guest");
		Guest guest = service.get(id);
		mav.addObject("guest", guest);
		
		return mav;
	}
	
	@RequestMapping("/delete/{id}")
	public String deleteGuest(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/guest";
	}
	
}
