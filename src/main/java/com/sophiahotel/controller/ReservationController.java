package com.sophiahotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sophiahotel.model.Reservation;
import com.sophiahotel.service.ReservationService;

@Controller
public class ReservationController {

	@Autowired
	private ReservationService service;
	
	@RequestMapping("/reservation")
	public String viewReservationPage(Model model) {
		List<Reservation> listReservation = service.listAll();
		model.addAttribute("listReservation", listReservation);
		return "reservationindex";
	}
	
	@RequestMapping("/newReservation")
	public String showReservation(Model model) {
		Reservation reservation = new Reservation();
		model.addAttribute("reservation", reservation);
		return "new_reservation";
	}
	
	@RequestMapping(value = "/saveReservation", method = RequestMethod.POST)
	public String saveReservation(@ModelAttribute("reservation") Reservation reservation) {
		service.save(reservation);
		return "redirect:/reservation";
	}
	
	@RequestMapping("/editReservation/{id}")
	public ModelAndView showEditReservationPage(@PathVariable(name = "id") int id) {
		ModelAndView mav = new ModelAndView("edit_reservation");
		Reservation reservation = service.get(id);
		mav.addObject("reservation", reservation);
		
		return mav;
	}
	
	@RequestMapping("/deleteReservation/{id}")
	public String deleteReservation(@PathVariable(name = "id") int id) {
		service.delete(id);
		return "redirect:/reservation";
	}
}
