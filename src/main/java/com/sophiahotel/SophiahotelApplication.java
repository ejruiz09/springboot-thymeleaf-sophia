package com.sophiahotel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SophiahotelApplication {

	public static void main(String[] args) {
		SpringApplication.run(SophiahotelApplication.class, args);
	}

}
