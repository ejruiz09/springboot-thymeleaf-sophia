package com.sophiahotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sophiahotel.model.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

}
