package com.sophiahotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sophiahotel.model.ReservedRoom;

public interface ReservedRoomRepository extends JpaRepository<ReservedRoom, Integer> {

}
