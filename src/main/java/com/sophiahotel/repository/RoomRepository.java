package com.sophiahotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sophiahotel.model.Room;

public interface RoomRepository extends JpaRepository<Room, Integer> {

}
