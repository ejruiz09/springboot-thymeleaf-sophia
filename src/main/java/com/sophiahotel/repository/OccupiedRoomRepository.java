package com.sophiahotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sophiahotel.model.OccupiedRoom;

public interface OccupiedRoomRepository extends JpaRepository<OccupiedRoom, Integer> {

}
