package com.sophiahotel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.sophiahotel.model.Guest;

public interface GuestRepository extends JpaRepository<Guest, Integer> {

}
