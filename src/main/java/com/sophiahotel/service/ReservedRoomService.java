package com.sophiahotel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sophiahotel.model.ReservedRoom;
import com.sophiahotel.repository.ReservedRoomRepository;

@Service
@Transactional
public class ReservedRoomService {

	@Autowired
	private ReservedRoomRepository repo;
	
	public List<ReservedRoom> listAll(){
		return repo.findAll();
	}
	
	public void save(ReservedRoom reservedRoom) {
		repo.save(reservedRoom);
	}
	
	public ReservedRoom get(int id) {
		return repo.findById(id).get();
	}
	
	public void delete(int id) {
		repo.deleteById(id);
	}
}
