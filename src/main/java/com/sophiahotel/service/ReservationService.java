package com.sophiahotel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sophiahotel.model.Reservation;
import com.sophiahotel.repository.ReservationRepository;

@Service
@Transactional
public class ReservationService {

	@Autowired
	private ReservationRepository repo;
	
	public List<Reservation> listAll(){
		return repo.findAll();
	}
	
	public void save(Reservation reservation) {
		repo.save(reservation);
	}
	
	public Reservation get(int id) {
		return repo.findById(id).get();
	}
	
	public void delete(int id) {
		repo.deleteById(id);
	}
}
