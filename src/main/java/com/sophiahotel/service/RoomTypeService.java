package com.sophiahotel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sophiahotel.model.RoomType;
import com.sophiahotel.repository.RoomTypeRepository;

@Service
@Transactional
public class RoomTypeService {

	@Autowired
	private RoomTypeRepository repo;
	
	public List<RoomType> listAll(){
		return repo.findAll();
	}
	
	public void save(RoomType roomtype) {
		repo.save(roomtype);
	}
	
	public RoomType get(int id) {
		return repo.findById(id).get();
	}
	
	public void delete(int id) {
		repo.deleteById(id);
	}
}
