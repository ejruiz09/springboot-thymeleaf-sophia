package com.sophiahotel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sophiahotel.model.HostedAt;
import com.sophiahotel.repository.HostedAtRepository;

@Service
@Transactional
public class HostedAtService {

	@Autowired
	private HostedAtRepository repo;
	
	public List<HostedAt> listAll(){
		return repo.findAll();
	}
	
	public void save(HostedAt hostedAt) {
		repo.save(hostedAt);
	}
	
	public HostedAt get(int id) {
		return repo.findById(id).get();
	}
	
	public void delete(int id) {
		repo.deleteById(id);
	}
}
