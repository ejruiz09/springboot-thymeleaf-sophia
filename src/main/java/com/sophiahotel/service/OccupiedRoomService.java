package com.sophiahotel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sophiahotel.model.OccupiedRoom;
import com.sophiahotel.repository.OccupiedRoomRepository;

@Service
@Transactional
public class OccupiedRoomService {

	@Autowired
	private OccupiedRoomRepository repo;
	
	public List<OccupiedRoom> listAll(){
		return repo.findAll();
	}
	
	public void save(OccupiedRoom occupiedRoom) {
		repo.save(occupiedRoom);
	}
	
	public OccupiedRoom get(int id) {
		return repo.findById(id).get();
	}
	
	public void delete(int id) {
		repo.deleteById(id);
	}
}
